<?php

namespace App\Http\Controllers;

use App\Models\bayi;
use Illuminate\Http\Request;

class BayiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bayi = Bayi::all();
        return view('bayi.index', compact('bayi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bayi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorebayiRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_lengkap'          =>  'required',
            'jenis_kelamin'         =>  'required',
            'alamat'                =>  'required',
            'nama_ayah'             =>  'required',
            'nama_ibu'              =>  'required',
            'tanggal_lahir'         =>  'required'
            
        ]);

        $bayi = new bayi;

        $bayi->nama_lengkap = $request->nama_lengkap;
        $bayi->jenis_kelamin = $request->jenis_kelamin;
        $bayi->alamat = $request->alamat;
        $bayi->nama_ayah = $request->nama_ayah;
        $bayi->tanggal_lahir = $request->tanggal_lahir;
        $bayi->nama_ibu = $request->nama_ibu;

        $bayi->save();

        return redirect()->route('bayi.index')->with('success', 'Data Berhasil Ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\bayi  $bayi
     * @return \Illuminate\Http\Response
     */
    public function show(bayi $bayi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\bayi  $bayi
     * @return \Illuminate\Http\Response
     */
    public function edit(bayi $bayi)
    {
        return view('bayi.edit', compact('bayi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatebayiRequest  $request
     * @param  \App\Models\bayi  $bayi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, bayi $bayi)
    {
        $this->validate($request, [
            'nama_lengkap'          =>  'required',
            'jenis_kelamin'         =>  'required',
            'alamat'                =>  'required',
            'nama_ayah'             =>  'required',
            'nama_ibu'              =>  'required',
            'tanggal_lahir'         =>  'required'
        ]);
    
        //get data Blog by ID
        $bayi = bayi::findOrFail($bayi->id);
        $bayi->nama_lengkap = $request->nama_lengkap;
        $bayi->jenis_kelamin = $request->jenis_kelamin;
        $bayi->alamat = $request->alamat;
        $bayi->nama_ayah = $request->nama_ayah;
        $bayi->tanggal_lahir = $request->tanggal_lahir;
        $bayi->nama_ibu = $request->nama_ibu;

        $bayi->save();

        return redirect()->route('bayi.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\bayi  $bayi
     * @return \Illuminate\Http\Response
     */
    public function destroy(bayi $bayi)
    {
        $bayi->delete();

        return redirect()->route('bayi.index')->with('success', 'Data Berhasil Dihapus');
    }
}
