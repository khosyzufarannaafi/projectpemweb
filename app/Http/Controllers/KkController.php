<?php

namespace App\Http\Controllers;

use App\Models\kk;
use Illuminate\Http\Request;


class KkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kk = Kk::all();
        return view('kk.index', compact('kk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorekkRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_lengkap'          =>  'required',
            'jenis_kelamin'         =>  'required',
            'alamat'                =>  'required'
            
        ]);

        $kk = new kk;

        $kk->nama_lengkap = $request->nama_lengkap;
        $kk->jenis_kelamin = $request->jenis_kelamin;
        $kk->alamat = $request->alamat;

        $kk->save();

        return redirect()->route('kk.index')->with('success', 'Data Berhasil Ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\kk  $kk
     * @return \Illuminate\Http\Response
     */
    public function show(kk $kk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\kk  $kk
     * @return \Illuminate\Http\Response
     */
    public function edit(kk $kk)
    {
        return view('kk.edit', compact('kk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatekkRequest  $request
     * @param  \App\Models\kk  $kk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, kk $kk)
    {
        $this->validate($request, [
            'nama_lengkap'          =>  'required',
            'jenis_kelamin'         =>  'required',
            'alamat'                =>  'required'
        ]);
    
        //get data Blog by ID
        $kk = kk::findOrFail($kk->id);
        $kk->nama_lengkap = $request->nama_lengkap;
        $kk->jenis_kelamin = $request->jenis_kelamin;
        $kk->alamat = $request->alamat;

        $kk->save();

        return redirect()->route('kk.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\kk  $kk
     * @return \Illuminate\Http\Response
     */
    public function destroy(kk $kk)
    {
        $kk->delete();

        return redirect()->route('kk.index')->with('success', 'Data Berhasil Dihapus');
    }
}
