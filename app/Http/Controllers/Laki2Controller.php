<?php

namespace App\Http\Controllers;

use App\Models\laki2;
use App\Http\Requests\Storelaki2Request;
use App\Http\Requests\Updatelaki2Request;

class Laki2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Storelaki2Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Storelaki2Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\laki2  $laki2
     * @return \Illuminate\Http\Response
     */
    public function show(laki2 $laki2)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\laki2  $laki2
     * @return \Illuminate\Http\Response
     */
    public function edit(laki2 $laki2)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Updatelaki2Request  $request
     * @param  \App\Models\laki2  $laki2
     * @return \Illuminate\Http\Response
     */
    public function update(Updatelaki2Request $request, laki2 $laki2)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\laki2  $laki2
     * @return \Illuminate\Http\Response
     */
    public function destroy(laki2 $laki2)
    {
        //
    }
}
