<?php

namespace App\Http\Controllers;

use App\Models\meninggal;
use App\Http\Requests\StoremeninggalRequest;
use App\Http\Requests\UpdatemeninggalRequest;

class MeninggalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoremeninggalRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoremeninggalRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\meninggal  $meninggal
     * @return \Illuminate\Http\Response
     */
    public function show(meninggal $meninggal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\meninggal  $meninggal
     * @return \Illuminate\Http\Response
     */
    public function edit(meninggal $meninggal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatemeninggalRequest  $request
     * @param  \App\Models\meninggal  $meninggal
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatemeninggalRequest $request, meninggal $meninggal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\meninggal  $meninggal
     * @return \Illuminate\Http\Response
     */
    public function destroy(meninggal $meninggal)
    {
        //
    }
}
