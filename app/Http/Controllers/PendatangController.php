<?php

namespace App\Http\Controllers;

use App\Models\pendatang;
use App\Http\Requests\StorependatangRequest;
use App\Http\Requests\UpdatependatangRequest;

class PendatangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorependatangRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorependatangRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\pendatang  $pendatang
     * @return \Illuminate\Http\Response
     */
    public function show(pendatang $pendatang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\pendatang  $pendatang
     * @return \Illuminate\Http\Response
     */
    public function edit(pendatang $pendatang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatependatangRequest  $request
     * @param  \App\Models\pendatang  $pendatang
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatependatangRequest $request, pendatang $pendatang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\pendatang  $pendatang
     * @return \Illuminate\Http\Response
     */
    public function destroy(pendatang $pendatang)
    {
        //
    }
}
