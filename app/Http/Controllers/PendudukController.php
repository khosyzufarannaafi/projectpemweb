<?php

namespace App\Http\Controllers;

use App\Models\penduduk;
use Illuminate\Http\Request;

class PendudukController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penduduk = Penduduk::all();
        return view('penduduk.index', compact('penduduk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penduduk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorependudukRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_lengkap'          =>  'required',
            'nama_panggilan'        =>  'required',
            'tempat_lahir'          =>  'required',
            'tanggal_lahir'         =>  'required',
            'jenis_kelamin'         =>  'required',
            'alamat'                =>  'required'
            
        ]);

        $penduduk = new penduduk;

        $penduduk->nama_lengkap = $request->nama_lengkap;
        $penduduk->nama_panggilan = $request->nama_panggilan;
        $penduduk->tempat_lahir = $request->tempat_lahir;
        $penduduk->tanggal_lahir = $request->tanggal_lahir;
        $penduduk->jenis_kelamin = $request->jenis_kelamin;
        $penduduk->alamat = $request->alamat;

        $penduduk->save();

        return redirect()->route('penduduk.index')->with('success', 'Data Berhasil Ditambahkan.');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\penduduk  $penduduk
     * @return \Illuminate\Http\Response
     */
    public function show(penduduk $penduduk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\penduduk  $penduduk
     * @return \Illuminate\Http\Response
     */
    public function edit(penduduk $penduduk)
    {
        return view('penduduk.edit', compact('penduduk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatependudukRequest  $request
     * @param  \App\Models\penduduk  $penduduk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, penduduk $penduduk)
    {
        $this->validate($request, [
            'nama_lengkap'          =>  'required',
            'nama_panggilan'        =>  'required',
            'tempat_lahir'          =>  'required',
            'tanggal_lahir'         =>  'required',
            'jenis_kelamin'         =>  'required',
            'alamat'                =>  'required'
        ]);
    
        //get data Blog by ID
        $penduduk = penduduk::findOrFail($penduduk->id);
        $penduduk->nama_lengkap = $request->nama_lengkap;
        $penduduk->nama_panggilan = $request->nama_panggilan;
        $penduduk->tempat_lahir = $request->tempat_lahir;
        $penduduk->tanggal_lahir = $request->tanggal_lahir;
        $penduduk->jenis_kelamin = $request->jenis_kelamin;
        $penduduk->alamat = $request->alamat;

        $penduduk->save();

        return redirect()->route('penduduk.index')->with(['success' => 'Data Berhasil Diupdate!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\penduduk  $penduduk
     * @return \Illuminate\Http\Response
     */
    public function destroy(penduduk $penduduk)
    {
        $penduduk->delete();

        return redirect()->route('penduduk.index')->with('success', 'Data Berhasil Dihapus');
    }
}
