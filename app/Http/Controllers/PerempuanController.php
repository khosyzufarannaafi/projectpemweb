<?php

namespace App\Http\Controllers;

use App\Models\perempuan;
use App\Http\Requests\StoreperempuanRequest;
use App\Http\Requests\UpdateperempuanRequest;

class PerempuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreperempuanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreperempuanRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\perempuan  $perempuan
     * @return \Illuminate\Http\Response
     */
    public function show(perempuan $perempuan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\perempuan  $perempuan
     * @return \Illuminate\Http\Response
     */
    public function edit(perempuan $perempuan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateperempuanRequest  $request
     * @param  \App\Models\perempuan  $perempuan
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateperempuanRequest $request, perempuan $perempuan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\perempuan  $perempuan
     * @return \Illuminate\Http\Response
     */
    public function destroy(perempuan $perempuan)
    {
        //
    }
}
