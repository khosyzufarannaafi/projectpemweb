<?php

namespace App\Http\Controllers;

use App\Models\pindah;
use App\Http\Requests\StorepindahRequest;
use App\Http\Requests\UpdatepindahRequest;

class PindahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorepindahRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorepindahRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\pindah  $pindah
     * @return \Illuminate\Http\Response
     */
    public function show(pindah $pindah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\pindah  $pindah
     * @return \Illuminate\Http\Response
     */
    public function edit(pindah $pindah)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatepindahRequest  $request
     * @param  \App\Models\pindah  $pindah
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatepindahRequest $request, pindah $pindah)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\pindah  $pindah
     * @return \Illuminate\Http\Response
     */
    public function destroy(pindah $pindah)
    {
        //
    }
}
