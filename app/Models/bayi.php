<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class bayi extends Model
{
    use HasFactory;

    protected $table = 'bayis';

    protected $fillable = ['nama_lengkap', 'nama_ayah' , 'nama_ibu' , 'tanggal_lahir', 'jenis_kelamin' ,'alamat'];
}
