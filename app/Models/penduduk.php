<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class penduduk extends Model
{
    use HasFactory;

    protected $table = 'penduduks';

    protected $fillable = ['nama_lengkap', 'nama_panggilan', 'tanggal_lahir', 'tempat_lahir' , 'jenis_kelamin' ,'alamat'];
}
