<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Data Bayi') }}
        </h2>
    </x-slot>

    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow rounded">
                    <div class="card-body">
                        <form action="{{ route('bayi.update', $bayi->id) }}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-group pb-3">
                                <label class="font-weight-bold">Nama Lengkap</label>
                                <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror"
                                    name="nama_lengkap" value="{{ old('nama_lengkap', $bayi->nama_lengkap) }}"
                                    placeholder="Masukkan Nama Lengkap">

                                <!-- error message untuk title -->
                                @error('nama_lengkap')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group pb-3">
                                <label class="font-weight-bold">Jenis Kelamin</label>
                                <input type="text" class="form-control @error('jenis_kelamin') is-invalid @enderror"
                                    name="jenis_kelamin" value="{{ old('jenis_kelamin', $bayi->jenis_kelamin) }}"
                                    placeholder="Masukkan Jenis Kelamin">

                                <!-- error message untuk title -->
                                @error('jenis_kelamin')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group pb-3">
                                <label class="font-weight-bold">Alamat</label>
                                <input type="text" class="form-control @error('alamat') is-invalid @enderror"
                                    name="alamat" value="{{ old('alamat', $bayi->alamat) }}"
                                    placeholder="Masukkan Nama Lengkap">

                                <!-- error message untuk title -->
                                @error('alamat')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group pb-3">
                                <label class="font-weight-bold">Nama Ayah</label>
                                <input type="text" class="form-control @error('nama_ayah') is-invalid @enderror"
                                    name="nama_ayah" value="{{ old('nama_ayah', $bayi->nama_ayah) }}"
                                    placeholder="Masukkan Nama Lengkap">

                                <!-- error message untuk title -->
                                @error('nama_ayah')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group pb-3">
                                <label class="font-weight-bold">Nama Ibu</label>
                                <input type="text" class="form-control @error('nama_ibu') is-invalid @enderror"
                                    name="nama_ibu" value="{{ old('nama_ibu', $bayi->nama_ibu) }}"
                                    placeholder="Masukkan Jenis Kelamin">

                                <!-- error message untuk title -->
                                @error('nama_ibu')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group pb-3">
                                <label class="font-weight-bold">Tanggal Lahir</label>
                                <input type="text" class="form-control @error('tanggal_lahir') is-invalid @enderror"
                                    name="tanggal_lahir" value="{{ old('tanggal_lahir', $bayi->tanggal_lahir) }}"
                                    placeholder="Masukkan Nama Lengkap">

                                <!-- error message untuk title -->
                                @error('tanggal_lahir')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="mt-3">
                                <button type="submit" class="btn btn-md btn-outline-primary">UPDATE</button>
                                <button type="reset" class="btn btn-md btn-outline-warning">RESET</button>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>