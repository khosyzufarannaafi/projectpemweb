<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Data Bayi') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if($message = Session::get('success'))

            <div class="alert alert-success">
                {{ $message }}
            </div>

            @endif
            <table class="table">
                <div class="text-end">
                    <a class="btn btn-success" href="{{ route('bayi.create') }}"">Tambah Data</a>
                </div>

                <thead>
                    <p class=" text-center fw-bold fs-1 mb-3">Data Bayi</p>

                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Lengkap</th>
                            <th scope="col">Jenis Kelamin</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">Nama Ayah</th>
                            <th scope="col">Nama Ibu</th>
                            <th scope="col">Tanggal Lahir</th>
                            <th colspan="2" scope="col">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @forelse ($bayi as $bayis)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $bayis->nama_lengkap }}</td>
                                <td>{{ $bayis->jenis_kelamin}}</td>
                                <td>{{ $bayis->alamat }}</td>
                                <td>{{ $bayis->nama_ayah }}</td>
                                <td>{{ $bayis->nama_ibu}}</td>
                                <td>{{ $bayis->tanggal_lahir }}</td>
                                <td class="text-center">
                                    <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                        action="{{ route('bayi.destroy', $bayis->id) }}" method="POST">
                                        <a href="{{ route('bayi.edit', $bayis->id) }}"
                                            class="btn btn-sm btn-primary">EDIT</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-outline-danger">HAPUS</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <div class="alert alert-danger">
                                Data Bayi belum Tersedia.
                            </div>
                            @endforelse
                        </tbody>
            </table>
        </div>
    </div>
</x-app-layout>