<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Kepala Keluarga') }}
        </h2>
    </x-slot>

    <div class="container">
        <div class="mt-5">
            @foreach($errors->all() as $error)
            <div class="alert alert-danger mt-0">
                <ul>
                    <li>{{ $error }}</li>
                </ul>
            </div>
            @endforeach
        </div>
        <div class="card mt-5">
            <div class="card-header">Tambah Data Kepala Keluarga</div>
            <div class="card-body">
                <form method="post" action="{{ route('kk.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="row mb-3">
                        <label class="col-sm-2 col-label-form">Nama Lengkap</label>
                        <div class="col-sm-10">
                            <input type="text" name="nama_lengkap" class="form-control" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-label-form">Jenis Kelamin</label>
                        <div class="col-sm-10">
                            <input type="text" name="jenis_kelamin" class="form-control" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-label-form">Alamat</label>
                        <div class="col-sm-10">
                            <input type="text" name="alamat" class="form-control" />
                        </div>
                    </div>
                    <div class="text-center">
                        <input type="submit" class="btn btn-outline-primary" value="Tambah Data" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>