<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Kepala Keluarga') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if($message = Session::get('success'))

            <div class="alert alert-success">
                {{ $message }}
            </div>

            @endif
            <table class="table">
                <div class="text-end">
                    <a class="btn btn-success" href="{{ route('kk.create') }}"">Tambah Data</a>
                </div>

                <thead>
                    <p class=" text-center fw-bold fs-1 mb-3">Data Kepala Keluarga</p>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Lengkap</th>
                            <th scope="col">Jenis Kelamin</th>
                            <th scope="col">Alamat</th>
                            <th colspan="2" scope="col">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @forelse ($kk as $kks)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $kks->nama_lengkap }}</td>
                                <td>{{ $kks->jenis_kelamin}}</td>
                                <td>{{ $kks->alamat }}</td>
                                <td class="text-center">
                                    <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                        action="{{ route('kk.destroy', $kks->id) }}" method="POST">
                                        <a href="{{ route('kk.edit', $kks->id) }}"
                                            class="btn btn-sm btn-primary">EDIT</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-outline-danger">HAPUS</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <div class="alert alert-danger">
                                Data Kepala Keluarga belum Tersedia.
                            </div>
                            @endforelse
                        </tbody>
            </table>
        </div>
    </div>
</x-app-layout>