<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Data Penduduk') }}
        </h2>
    </x-slot>

    <div class="container mt-5 mb-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow rounded">
                    <div class="card-body">
                        <form action="{{ route('penduduk.update', $penduduk->id) }}" method="POST">
                            @csrf
                            @method('PUT')

                            <div class="form-group pb-3">
                                <label class="font-weight-bold">Nama Lengkap</label>
                                <input type="text" class="form-control @error('nama_lengkap') is-invalid @enderror"
                                    name="nama_lengkap" value="{{ old('nama_lengkap', $penduduk->nama_lengkap) }}"
                                    placeholder="Masukkan Nama Lengkap">

                                <!-- error message untuk title -->
                                @error('nama_lengkap')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group pb-3">
                                <label class="font-weight-bold">Nama Panggilan</label>
                                <input type="text" class="form-control @error('nama_panggilan') is-invalid @enderror"
                                    name="nama_panggilan" value="{{ old('nama_panggilan', $penduduk->nama_panggilan) }}"
                                    placeholder="Masukkan Nama Lengkap">

                                <!-- error message untuk title -->
                                @error('nama_panggilan')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group pb-3">
                                <label class="font-weight-bold">Tempat Lahir</label>
                                <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror"
                                    name="tempat_lahir" value="{{ old('tempat_lahir', $penduduk->tempat_lahir) }}"
                                    placeholder="Masukkan Nama Lengkap">

                                <!-- error message untuk title -->
                                @error('tempat_lahir')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group pb-3">
                                <label class="font-weight-bold">Tanggal Lahir</label>
                                <input type="text" class="form-control @error('tanggal_lahir') is-invalid @enderror"
                                    name="tanggal_lahir" value="{{ old('tanggal_lahir', $penduduk->tanggal_lahir) }}"
                                    placeholder="Masukkan Nama Lengkap">

                                <!-- error message untuk title -->
                                @error('tanggal_lahir')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group pb-3">
                                <label class="font-weight-bold">Jenis Kelamin</label>
                                <input type="text" class="form-control @error('jenis_kelamin') is-invalid @enderror"
                                    name="jenis_kelamin" value="{{ old('jenis_kelamin', $penduduk->jenis_kelamin) }}"
                                    placeholder="Masukkan Nama Lengkap">

                                <!-- error message untuk title -->
                                @error('jenis_kelamin')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="form-group pb-3">
                                <label class="font-weight-bold">Alamat</label>
                                <input type="text" class="form-control @error('alamat') is-invalid @enderror"
                                    name="alamat" value="{{ old('alamat', $penduduk->alamat) }}"
                                    placeholder="Masukkan Nama Lengkap">

                                <!-- error message untuk title -->
                                @error('alamat')
                                <div class="alert alert-danger mt-2">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>

                            <div class="mt-3">
                                <button type="submit" class="btn btn-md btn-outline-primary">UPDATE</button>
                                <button type="reset" class="btn btn-md btn-outline-warning">RESET</button>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>