<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Data Penduduk') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if($message = Session::get('success'))

            <div class="alert alert-success">
                {{ $message }}
            </div>

            @endif
            <table class="table">
                <div class="text-end">
                    <a class="btn btn-success" href="{{ route('penduduk.create') }}"">Tambah Data</a>
                </div>

                <thead>
                    <p class=" text-center fw-bold fs-1 mb-3">Data Penduduk</p>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama Lengkap</th>
                            <th scope="col">Nama Panggilan</th>
                            <th scope="col">Tempat Lahir</th>
                            <th scope="col">Tanggal Lahir</th>
                            <th scope="col">Jenis Kelamin</th>
                            <th scope="col">Alamat</th>
                            <th colspan="2" scope="col">Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @php $no = 1; @endphp
                            @forelse ($penduduk as $penduduks)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $penduduks->nama_lengkap }}</td>
                                <td>{{ $penduduks->nama_panggilan }}</td>
                                <td>{{ $penduduks->tempat_lahir }}</td>
                                <td>{{ $penduduks->tanggal_lahir }}</td>
                                <td>{{ $penduduks->jenis_kelamin }}</td>
                                <td>{{ $penduduks->alamat }}</td>
                                <td class="text-center">
                                    <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                        action="{{ route('penduduk.destroy', $penduduks->id) }}" method="POST">
                                        <a href="{{ route('penduduk.edit', $penduduks->id) }}"
                                            class="btn btn-sm btn-primary">EDIT</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-outline-danger">HAPUS</button>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <div class="alert alert-danger">
                                Data Penduduk belum Tersedia.
                            </div>
                            @endforelse
                        </tbody>
            </table>
        </div>
    </div>
</x-app-layout>